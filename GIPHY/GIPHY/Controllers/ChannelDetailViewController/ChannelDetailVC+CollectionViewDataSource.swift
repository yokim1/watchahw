//
//  ChannelDetailVC+.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/06/11.
//

import UIKit

//MARK: - UICollectionView Data Source
extension ChannelDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10 // need to put number of real data
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChannelDetailListCell.identifier, for: indexPath) as? ChannelDetailListCell else { return UICollectionViewCell() }
        //cell.image.image
            return cell
    }
}
