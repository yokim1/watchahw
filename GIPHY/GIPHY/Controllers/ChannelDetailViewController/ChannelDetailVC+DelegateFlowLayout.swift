//
//  ChannelDetailVC+.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/06/11.
//

import UIKit

//MARK: - CollectionView Delegate Flow Layout
extension ChannelDetailViewController: PinterestLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        //MARK: - Leave this comment for future use
//        guard var height: CGFloat = NumberFormatter().number(from: SearchResultDatas[indexPath.item].images.downsized_large.height) as? CGFloat else {
//            return CGFloat() }
        var height: CGFloat = 300
        let numberOfColumns: CGFloat = 2
        let yInsets: CGFloat = 10
        let cellSpacing: CGFloat = 3
        
        height = (height / numberOfColumns) - yInsets + cellSpacing
        return height
    }
}
