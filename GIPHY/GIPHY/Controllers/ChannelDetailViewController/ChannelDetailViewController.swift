//
//  ChannelDetailViewController.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/28.
//

import UIKit

class ChannelDetailViewController: UIViewController {

    static let identifier = "ChannelDetailViewController"
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        if let layout = collectionView.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
        fetchImages()
        
        configure()
    }
    
    //MARK: - Views
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var channelProfileImage: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBAction func backButtonDidTapped(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    //MARK: Data
    var channelData: ChannelData?
}

private extension ChannelDetailViewController {
    func configure() {
        if let url = channelData?.user?.banner_url {
            NetworkService.shared.loadImageForCaching(urlString: url) {[weak self] image in
                DispatchQueue.main.async {
                    self?.bannerImage.image = image
                }
            }
        }
        
        if let url = channelData?.user?.avatar_url {
            NetworkService.shared.loadImageForCaching(urlString: url) {[weak self] image in
                DispatchQueue.main.async {
                    self?.channelProfileImage.image = image
                }
            }
        }
        
        profileNameLabel.text = channelData?.user?.display_name
        userNameLabel.text = "@\(channelData?.user?.username ?? "")"
    }
}

extension ChannelDetailViewController {
    func fetchImages () {
        let urlString = ""
        NetworkService.shared.fetchData(urlString: urlString, type: SearchResultModel.self) {[weak self] error, fetchedData in
            //guard let data = fetchedData?.data else { return }
            //self.SearchResultDatas = data
            
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }
    }
}

