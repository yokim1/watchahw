//
//  SearchVC.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/06/11.
//

import UIKit
//MARK: - TableView Delegate
extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == tagSearchSection {
            guard let vc = storyboard?.instantiateViewController(identifier: SearchResultViewController.identifier) as? SearchResultViewController else {return }
            vc.searchedTarget = tagDatas[indexPath.row].name ?? ""
            vc.targetDataType = self.targetDataType
            vc.navigationItem.title = tagDatas[indexPath.row].name ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            self.dismiss(animated: false, completion: nil)
        }
        else if indexPath.section == channelSearchSection {
            guard let vc = storyboard?.instantiateViewController(identifier: ChannelDetailViewController.identifier) as? ChannelDetailViewController else {return }
            vc.channelData = self.channelDatas[indexPath.row]
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)
        }
        
        tableView.cellForRow(at: indexPath)?.isSelected = false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == channelSearchSection {
            return heightForRowAtChannelSearchSection
        }
        return heightForRowAtKeyWordSearchSection
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == channelSearchSection {
            return channelDatas.count == 0 ? 0 : heightForHeaderInSection
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = .black
            headerView.backgroundView?.backgroundColor = .black
            headerView.textLabel?.textColor = .white
            headerView.textLabel?.text = "Channel"
        }
    }
}
