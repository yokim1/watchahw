//
//  Search2ViewController.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/30.
//

import Foundation
//
//  ViewController.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/27.
//

import UIKit
class SearchViewController: UIViewController {
    
    static let identifier = "SearchViewController"
    
    //MARK: - ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        tableView.register(ChannelPreviewDataTableViewCell.nib(), forCellReuseIdentifier: ChannelPreviewDataTableViewCell.identifier)
        
        tableView.dataSource = self
        tableView.delegate = self
        searchTextField.delegate = self
        searchTextField.text = searchedTarget
        
        fetchDataForTagsAndChannelPreview(for: self.searchedTarget)
        self.navigationItem.hidesBackButton = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        setSegmentControlPosition()
    }
    
    //MARK: - TableView Section and Row Height
    let heightForHeaderInSection: CGFloat = 50
    let heightForRowAtChannelSearchSection: CGFloat = 60
    let heightForRowAtKeyWordSearchSection: CGFloat = 50
    
    //MARK: - TableView Section Numbers
    let tagSearchSection = 0
    let channelSearchSection = 1
    let numberOfSections = 2
    
    //MARK: - ViewController Datas
    var tagDatas: [TagData] = []
    var channelDatas: [ChannelData] = []
    var targetDataType: TargetDataType = .gifs
    var searchedTarget: String = ""
    
    //MARK: - View
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBAction func segmentControlDidTapped(_ sender: Any) {
        switch segmentControl.selectedSegmentIndex {
        case 0: targetDataType = .gifs
            break
        case 1: targetDataType = .stickers
            break
        default:
            break
        }
    }
    @IBAction func searchButtonDidTapped(_ sender: Any) {
        guard let searchResultViewController = storyboard?.instantiateViewController(identifier: SearchResultViewController.identifier) as? SearchResultViewController else {return }
        
        searchResultViewController.searchedTarget = searchTextField.text ?? ""
        searchResultViewController.targetDataType = self.targetDataType
        
        self.navigationController?.pushViewController(searchResultViewController, animated: true)
        
        self.dismiss(animated: false, completion: nil)
    }
}

//MARK: - KeyBoard Release
extension SearchViewController {
    private func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

extension SearchViewController {
   func fetchDataForTagsAndChannelPreview(for searchText: String) {
        
        let urlStringForTags = "https://api.giphy.com/v1/gifs/search/tags?q=\(searchText)\(apiKey)"
        NetworkService.shared.fetchData(urlString: urlStringForTags, type: TagDataResultModel.self) {[weak self] error, fetchedData in
            guard let data = fetchedData?.data else {return}
            
            DispatchQueue.main.async {
                self?.tagDatas = data
                self?.tableView.reloadData()
            }
        }
        
        let urlStringForChannels = "https://api.giphy.com/v1/channels/search?q=\(searchText)\(apiKey)"
        NetworkService.shared.fetchData(urlString: urlStringForChannels, type: ChannelModel.self) {[weak self] error, fetchedData in
            guard let data = fetchedData?.data else {return }
            
            DispatchQueue.main.async {
                self?.channelDatas = data
                self?.tableView.reloadData()
            }
        }
    }
}

extension SearchViewController {
    private func setSegmentControlPosition() {
        switch targetDataType {
        case .gifs:
            segmentControl.selectedSegmentIndex = 0
            break
            
        case .stickers:
            segmentControl.selectedSegmentIndex = 1
            break
        case .channel:
            break
        }
    }
}


