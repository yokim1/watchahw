//
//  SearchVC+.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/06/11.
//

import UIKit

extension SearchViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            searchedTarget = text + string
            fetchDataForTagsAndChannelPreview(for: text + string)
        }
        return true
    }
}
