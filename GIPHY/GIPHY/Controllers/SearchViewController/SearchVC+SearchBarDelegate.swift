//
//  SearchVC+.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/06/11.
//

import UIKit

//MARK: - SearchBar Delegate
extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchResultViewController = storyboard?.instantiateViewController(identifier: SearchResultViewController.identifier) as? SearchResultViewController else {return }
        
        searchResultViewController.searchedTarget = searchBar.text ?? ""
        searchResultViewController.targetDataType = self.targetDataType
        
        self.navigationController?.pushViewController(searchResultViewController, animated: true)
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        switch selectedScope {
        case 0: targetDataType = .gifs
            break
        case 1: targetDataType = .stickers
            break
        default:
            break
        }
    }
}
