//
//  SearchVC+.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/06/11.
//

import UIKit

//MARK: - TableView Data Source
extension SearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == tagSearchSection {
            return tagDatas.count
        }
        if section == channelSearchSection {
            return channelDatas.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == tagSearchSection {
            guard indexPath.row <= tagDatas.count,
            let keyWordSearchListCell = tableView.dequeueReusableCell(withIdentifier: KeyWordSearchListCell.identifier, for: indexPath) as? KeyWordSearchListCell else {return UITableViewCell()}
            keyWordSearchListCell.configure(with: tagDatas[indexPath.row])
            return keyWordSearchListCell
        }
        else if indexPath.section == channelSearchSection {
            guard indexPath.row <= channelDatas.count,
            let channelPreviewDataTableViewCell = tableView.dequeueReusableCell(withIdentifier: ChannelPreviewDataTableViewCell.identifier, for: indexPath) as? ChannelPreviewDataTableViewCell else { return UITableViewCell()}
            channelPreviewDataTableViewCell.configure(with: channelDatas[indexPath.row])
            return channelPreviewDataTableViewCell
        }
        return UITableViewCell()
    }
}
