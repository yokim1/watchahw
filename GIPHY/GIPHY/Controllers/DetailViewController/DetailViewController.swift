//
//  DetailViewController.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/29.
//

import UIKit

class DetailViewController: UIViewController {
    
    static let identifier = "DetailViewController"
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LikedImageManager.shared.likedImagesInformation = Storage.retrive("LikedImageId.json", from: .documents, as: LikedImageInformation.self) ?? LikedImageInformation()
        
        if let id = data?.id {
            if LikedImageManager.shared.likedImagesInformation.id[id] != nil {
                likeButton.isSelected = true
            }
        }
        
        guard let imageId = self.data?.images.original.url else {return }
        NetworkService.shared.loadImageForCaching(urlString: "\(imageId)?\(apiKey)") {[weak self] image in
            DispatchQueue.main.async {
                self?.image.image = image
            }
        }
        
        let tappedToLikeImage = UITapGestureRecognizer(target: self, action: #selector(imageDidTapped))
        tappedToLikeImage.numberOfTapsRequired = 2
        image.addGestureRecognizer(tappedToLikeImage)
        image.isUserInteractionEnabled = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(false)
        
        if likeButton.isSelected == false {
            guard let id = data?.id else {return }
            LikedImageManager.shared.likedImagesInformation.id.removeValue(forKey: id)
        }
        
        Storage.store(LikedImageManager.shared.likedImagesInformation, to: .documents, as: "LikedImageId.json")
    }
    
    //MARK: - VIews
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBAction func likeButtonDidTapped(_ sender: Any) {
        likeButton.isSelected = !likeButton.isSelected
        saveForLikedImages()
    }
    
    //MARK: - Data
    var data: DataDetail?
}

private extension DetailViewController {
    @objc func imageDidTapped() {
        likeButton.isSelected = !likeButton.isSelected
        saveForLikedImages()
    }
    
    func saveForLikedImages() {
        guard let id = data?.id else { return }
        LikedImageManager.shared.likedImagesInformation.id.updateValue(id, forKey: id)
    }
}
