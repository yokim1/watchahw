//
//  SearchResultVC+.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/06/11.
//

import UIKit

//MARK: - CollectionView Delegate
extension SearchResultViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(identifier: DetailViewController.identifier) as? DetailViewController else {return}
        vc.data = SearchResultDatas[indexPath.item]
        vc.navigationItem.title = targetDataType.rawValue
        navigationController?.pushViewController(vc, animated: true)
    }
}
