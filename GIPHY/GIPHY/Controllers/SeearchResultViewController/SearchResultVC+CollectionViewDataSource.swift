//
//  SearchResultVC+.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/06/11.
//

import UIKit

//MARK: - CollectionView Data Source
extension SearchResultViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SearchResultDatas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchedResultCell.identifier, for: indexPath) as? SearchedResultCell else {return UICollectionViewCell() }
        cell.configure(with: SearchResultDatas[indexPath.item])
        return cell
    }
}
