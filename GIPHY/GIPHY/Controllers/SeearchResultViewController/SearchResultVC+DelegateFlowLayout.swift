//
//  SearchResultVC+.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/06/11.
//

import UIKit
//MARK: - CollectionView Delegate Flow Layout
extension SearchResultViewController: PinterestLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        guard let imageHeightString = SearchResultDatas[indexPath.item].images.downsized_large.height else {return CGFloat()}
        guard var height: CGFloat = NumberFormatter().number(from: imageHeightString) as? CGFloat else {
            return CGFloat() }
        let numberOfColumns: CGFloat = 2
        let yInsets: CGFloat = 10
        let cellSpacing: CGFloat = 3
        
        height = (height / numberOfColumns) - yInsets + cellSpacing
        return height
    }
}
