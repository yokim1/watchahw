//
//  SearchResultVC+.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/06/11.
//

import UIKit

//MARK: - TextField Delegate
extension SearchResultViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let vc = storyboard?.instantiateViewController(identifier: SearchViewController.identifier) as? SearchViewController else { return }
        vc.searchedTarget = searchTextField.text ?? ""
        vc.targetDataType = targetDataType
        vc.modalPresentationStyle = .currentContext
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
