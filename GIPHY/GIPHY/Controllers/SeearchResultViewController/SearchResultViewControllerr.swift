//
//  GifDetailViewController.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/28.
//

import UIKit

class SearchResultViewController: UIViewController {
    
    static let identifier = "SearchResultViewController"
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        searchTextField.delegate = self
        if let layout = collectionView.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
        searchTextField.text = searchedTarget
        
        fetchImages()
        setSegmentControlPosition()
    }
    
    //MARK: - Views
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBAction func segmentControlDidTapped (_ sender: Any) {
        selectTargeDataType(segmentControl.selectedSegmentIndex)
        bringImagesDirectedBySegmentController()
    }
    
    //MARK: - Data
    var SearchResultDatas: [DataDetail] = []
    var targetDataType: TargetDataType = .gifs
    var searchedTarget: String = ""
}

//MARK: - For keyboard dismiss
extension SearchResultViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

//MARK: - Fetch Images When VC is Initiated
extension SearchResultViewController {
    func fetchImages () {
        let urlString = "https://api.giphy.com/v1/\(targetDataType)/search?q=\(searchedTarget)&\(apiKey)"
        NetworkService.shared.fetchData(urlString: urlString, type: SearchResultModel.self) {[weak self] error, fetchedData in
            guard let data = fetchedData?.data else { return }
            self?.SearchResultDatas = data
            
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }
    }
}

private extension SearchResultViewController {
    func selectTargeDataType(_ selectedScope: Int){
        switch selectedScope {
        case 0: targetDataType = .gifs
            break
        case 1: targetDataType = .stickers
            break
        default:
            break
        }
    }
}

//MARK: - Segment Control Setting
private extension SearchResultViewController {
    func setSegmentControlPosition() {
        switch targetDataType {
        case .gifs:
            segmentControl.selectedSegmentIndex = 0
            break
            
        case .stickers:
            segmentControl.selectedSegmentIndex = 1
            break
        case .channel:
            break
        }
    }
}

private extension SearchResultViewController {
    func bringImagesDirectedBySegmentController(){
        let urlString = "https://api.giphy.com/v1/\(targetDataType)/search?q=\(searchedTarget)\(apiKey)"
        NetworkService.shared.fetchData(urlString: urlString, type: SearchResultModel.self) {[weak self] error, fetchedData in
            guard let data = fetchedData?.data else { return }
            self?.SearchResultDatas = data
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }
    }
}
