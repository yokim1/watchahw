//
//  Service.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/27.
//

import UIKit

let apiKey = "&api_key=RRt6mM7jqKH6HiOboTo5Z4J1KYTLHqlo"

class NetworkService {
    static let shared = NetworkService()
    
    func fetchData<T: Decodable>(urlString: String, type: T.Type, completionHandler: @escaping (_ error: Error?, _ fetchedData: T?) -> ()) {
        guard let urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
              let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {return }
            do {
                let fetchedData = try JSONDecoder().decode(T.self, from: data)
                completionHandler(nil, fetchedData)
            } catch let error {
                print(error)
                print(error.localizedDescription)
                completionHandler(error, nil)
                return
            }
        }.resume()
    }
    
    func loadImageForCaching(urlString: String, completionHandler: @escaping (_ image: UIImage?)-> Void) {
        
        imageUrlString = urlString
        
        guard let url = URL(string: urlString) else { return }

        if let imageFromCache = imageCache.object(forKey: urlString as NSString) {

            completionHandler(imageFromCache)
            return
        }
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, respones, error) in
            
            if error != nil {
                print(error ?? "")
                return
            }
            
            DispatchQueue.main.async {
                guard let imageToCache = UIImage(data: data!) else { return }
                completionHandler(imageToCache)
                self.imageCache.setObject(imageToCache, forKey: urlString as NSString)
            }
        }).resume()
    }
    
    fileprivate let imageCache = NSCache<NSString, UIImage>()

    fileprivate var imageUrlString: String?

}
