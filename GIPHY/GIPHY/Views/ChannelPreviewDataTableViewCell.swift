//
//  ChannelPreviewDataTableViewCell.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/28.
//

import UIKit

class ChannelPreviewDataTableViewCell: UITableViewCell {
    
    static let identifier = "ChannelPreviewDataTableViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "ChannelPreviewDataTableViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

        channelProfileImage?.image = nil
        channelDisplayNameLabel.text?.removeAll()
        channelUserNameLabel.text?.removeAll()
    }

    func configure(with data: ChannelData) {
        guard let user = data.user else {return }
        NetworkService.shared.loadImageForCaching(urlString: user.avatar_url) {[weak self] image in
            self?.channelProfileImage.image = image
        }
        channelDisplayNameLabel.text = user.display_name
        channelUserNameLabel.text = "@\(user.username)"
    }
    
    @IBOutlet var channelProfileImage: UIImageView!
    @IBOutlet var channelDisplayNameLabel: UILabel!
    @IBOutlet var channelUserNameLabel: UILabel!
}

