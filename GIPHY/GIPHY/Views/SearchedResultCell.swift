//
//  Cell.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/29.
//
import UIKit

class SearchedResultCell: UICollectionViewCell {
    
    static let identifier = "SearchedResultCell"
    
    override func prepareForReuse() {
        super.prepareForReuse()
        image.image = nil
    }
    
    func configure(with data: DataDetail) {
        configureImage(toCache: image, at: data)
    }
    
    @IBOutlet weak var image: UIImageView!
}



