//
//  File.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/28.
//

import UIKit

class KeyWordSearchListCell: UITableViewCell {
    
    static let identifier = "KeyWordSearchListCell"
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        searchKeywordResult.text?.removeAll()
    }
    
    func configure(with data: TagData) {
        searchKeywordResult.text = data.name
    }
    
    @IBOutlet weak var searchKeywordResult: UILabel!
}
