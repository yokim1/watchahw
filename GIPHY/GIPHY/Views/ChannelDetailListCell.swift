//
//  ChannelDetailListCell.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/30.
//

import UIKit

class ChannelDetailListCell: UICollectionViewCell{
    
    static let identifier = "ChannelDetailListCell"
    
    override func prepareForReuse() {
        super.prepareForReuse()
        image.image = nil
    }
    
    func configure(with data: DataDetail){
        configureImage(toCache: image, at: data)
    }
    
    @IBOutlet weak var image: UIImageView!
}
