//
//  UICollectionViewExtension.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/30.
//

import UIKit

extension UICollectionViewCell {
    func configureImage(toCache imageForCaching: UIImageView, at data: DataDetail ) {
        guard let url = data.images.downsized.url else { return }
        
        NetworkService.shared.loadImageForCaching(urlString: url) { image in
            imageForCaching.image = image
        }
    }
}
