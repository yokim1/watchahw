//
//  TargetDataType.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/29.
//

import Foundation

enum TargetDataType: String {
    case gifs = "gifs"
    case stickers = "stickers"
    case channel = "channel"
}
