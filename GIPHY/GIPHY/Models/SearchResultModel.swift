//
//  Model.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/27.
//

import Foundation
//trending
struct SearchResultModel: Decodable{
    let data: [DataDetail]
}

struct DataDetail: Decodable {
    let type: String
    let id: String
    let slug: String
    let url: String
    let bitly_url: String
    let embed_url: String
    let username: String
    let source: String
    let rating: String
    let content_url: String
    let user: User?
    let source_tld: String
    let source_post_url: String
    let update_datetime: String?
    let create_datetime: String?
    let import_datetime: String?
    let trending_datetime: String
    let images: Images
    let title: String
    let analytics_response_payload: String
    let analytics: Analytic
}

struct Analytic: Decodable {
    let onload: Url
    let onclick: Url
    let onsent: Url
}

struct Url: Decodable {
    let url: String
}

struct Images: Decodable {
    let original: Original
    let downsized: Original
    let downsized_large: Original
    let downsized_medium: Original? 
    let downsized_small: Original
    let downsized_still: Original
}

struct Original: Decodable {
    let height: String?
    let width: String?
    let size: String?
    let url: String?
    let mp4_size: String?
    let mp4: String?
    let webp_size: String?
    let webp: String?
    let frames: String?
    let hash: String?
}

struct User: Decodable {
    let avatar_url: String
    let banner_image: String?
    let banner_url: String?
    let profile_url: String
    let username: String
    let display_name: String
    let description: String
    let instagram_url: String
    let website_url: String
    let is_verified: Bool
}
