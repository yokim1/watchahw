//
//  ChannelModel.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/27.
//

import Foundation

struct ChannelModel: Decodable {
    let data: [ChannelData]
    let pagination: Pagination
    let meta: Meta
}

struct ChannelData: Decodable {
    let id: Int
    let display_name: String
    let parent: Int?
    let slug: String?
    let type: String?
    let content_type: String?
    let short_display_name: String?
    let description: String
    let has_children: Bool
    let featured_gif: FeaturedGif?
    let banner_image: String?
    let user: User?
    let ancestors: [ChannelData]?
    let tags:[ChannelTag]?
    let analytics_response_payload: String?
}

struct FeaturedGif: Decodable{
    let type: String
    let id: String
    let url: String
    let slug: String
    let bitly_gif_url: String
    let bitly_url: String
    let embed_url: String
    let username: String
    let source: String
    let title: String
    let rating: String
    let content_url: String
    let source_tld: String
    let source_post_url: String
    let is_sticker: Int
    let import_datetime: String
    let trending_datetime: String
    let images: Images?
    let user: User?
}

struct ChannelTag: Decodable {
    let id: Int?
    let channel: Int?
    let rank: Int?
    let tag: String?
}

// MARK: - Pagination
struct Pagination: Codable {
    let totalCount, count, offset: Int?

    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case count, offset
    }
}

// MARK: - Meta
struct Meta: Codable {
    let status: Int
    let msg, responseID: String

    enum CodingKeys: String, CodingKey {
        case status, msg
        case responseID = "response_id"
    }
}
