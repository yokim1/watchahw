//
//  TagDataResultModel.swift
//  GIPHY
//
//  Created by 김윤석 on 2021/05/27.
//

import Foundation

struct TagDataResultModel: Decodable {
    let data: [TagData]
    let pagination: Pagination
    let meta: Meta
}

struct TagData: Decodable {
    let name: String?
    let analytics_response_payload: String
}
